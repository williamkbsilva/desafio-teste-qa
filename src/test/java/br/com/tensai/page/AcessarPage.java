package br.com.tensai.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import br.com.tensai.core.CoreDriver;
import br.com.tensai.core.CorePage;
import br.com.tensai.util.Property;


public class AcessarPage extends CorePage<AcessarPage> {

	protected WebDriver driver;
	
	public AcessarPage() {
		this.driver = CoreDriver.getDriver();
		PageFactory.initElements(CoreDriver.getDriver(), this);
	}
	
	
	public void AcessarURL() {
		driver.navigate().to(Property.URL);
		esperaForcada();
		
	}
	

	
}
