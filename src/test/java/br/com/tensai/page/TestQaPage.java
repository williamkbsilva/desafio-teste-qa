	package br.com.tensai.page;
	
	import org.junit.Assert;
	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.testng.annotations.Test;
	
	import br.com.tensai.core.CorePage;
	
	/**
	 * 
	 * @author william
	 *
	 */
	
	public class TestQaPage extends CorePage<TestQaPage> {
	
		AcessarPage acessa = new AcessarPage();
	
		
		
		//MAPEAMENTO DOS ELEMENTOS DA PÁGINA DE FORMULARIO
		@FindBy(xpath = "/html/body/div[3]/div/div[1]/div/div[2]/ul/li/ul/li[1]/a")
		private WebElement selecionarTipoFormulario;
		
		@FindBy(xpath="/html/body/div[3]/div/div[1]/div/div[2]/ul/li/ul/li[1]/ul/li[5]/a")
		private WebElement selecionarFormulario;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[1]/div/div/input")
		private WebElement campoPrimeiroNome;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[2]/div/div/input")
		private WebElement campoUltimoNome;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[3]/div/div/input")
		private WebElement campoEmail;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[4]/div/div/input")
		private WebElement campoTelefone;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[5]/div/div/input")
		private WebElement campoEndereco;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[6]/div/div/input")
		private WebElement campoCidade;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[7]/div/div/select")
		private WebElement btnEstado;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[7]/div/div/select/option[20]")
		private WebElement btnSelecionarEstado;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[8]/div/div/input")
		private WebElement campoCep;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[9]/div/div/input")
		private WebElement campoSite;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[10]/div/div[1]/label/input")
		private WebElement btnHospedagem;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[11]/div/div/textarea")
		private WebElement campoDescricaoProjeto;
		
		@FindBy(xpath="/html/body/div[2]/div/div[2]/section/form/fieldset/div[13]/div/button")
		private WebElement btnEnviar;
		
		
		
		
		
		
		
		public void selecionarTipoFormulario() {
			this.selecionarTipoFormulario.click();
		}
		
		public void selecionarFormulario() {
			this.selecionarFormulario.click();
		}
		
		public void primeiroNome(String texto) {
			this.preencherCampoClear(campoPrimeiroNome, texto);
		}
		
		public void ultimoNome(String texto) {
			this.preencherCampoClear(campoUltimoNome, texto);
		}
		
		public void email(String texto) {
			this.preencherCampoClear(campoEmail, texto);
		}
		
		public void telefone(String texto) {
			this.preencherCampoClear(campoTelefone, texto);
		}
		
		public void endereço(String texto) {
			this.preencherCampoClear(campoEndereco, texto);
		}
		
		public void cidade(String texto) {
			this.preencherCampoClear(campoCidade, texto);
		}
		
		public void estado() {
			this.btnEstado.click();
		}
		
		public void selecionaEstado() {
			this.btnSelecionarEstado.click();
		}
		
		public void cep(String texto) {
			this.preencherCampoClear(campoCep, texto);
		}
		
		public void site(String texto) {
			this.preencherCampoClear(campoSite, texto);
		}
		
		public void btnHospedagem() {
			this.btnHospedagem.click();
		}
		
		public void enviar() {
			this.btnEnviar.click();
		}
		
		
		// TESTES DE CADASTROS IMPLEMENTADOS
		
		
		//metodo que realiza um cadastro de formulario simples
		@Test
		public void formularioSimples() {
			acessa.AcessarURL();
			selecionarTipoFormulario();
			selecionarFormulario();
			primeiroNome("william");
			ultimoNome("kevin");
			email("teste@gmail.com");
			telefone("987683042");
			endereço("rua francisco de souza rangel");
			cidade("João Pessoa");
			estado();
			selecionaEstado();
			cep("58015730");
			site("www.teste.com.br");
			btnHospedagem();
			enviar();
		}
		
		
		
	}