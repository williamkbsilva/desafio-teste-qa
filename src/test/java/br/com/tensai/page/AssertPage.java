package br.com.tensai.page;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.Test;

import br.com.tensai.core.CorePage;

public class AssertPage extends CorePage<AssertPage> {

	AcessarPage acessa = new AcessarPage();

	// MAPEAMENTO DOS ELEMENTOS DA PÁGINA DE FORMULARIO
	@FindBy(xpath = "/html/body/div[3]/div/div[1]/div/div[2]/ul/li/ul/li[1]/a")
	private WebElement selecionarTipoFormulario;

	@FindBy(xpath = "//*[@id=\"treemenu\"]/li/ul/li[1]/ul/li[6]/a")
	private WebElement selecionarFormulario;

	@FindBy(xpath = "//*[@id=\"title\"]")
	private WebElement campoNome;

	@FindBy(xpath = "//*[@id=\"description\"]")
	private WebElement campoComentario;
	
	@FindBy(xpath="//*[@id=\"btn-submit\"]")
	private WebElement btnEnviar;
	
	@FindBy(xpath="//*[@id=\"submit-control\"]")
	private WebElement verificarResultado;
	
	
	public void selecionarTipoFormulario() {
		this.selecionarTipoFormulario.click();
	}
	
	public void selecionarFormulario() {
		this.selecionarFormulario.click();
	}
	
	public void campoNome(String texto) {
		this.preencherCampoClear(campoNome, texto);
	}
	
	public void campoComentario(String texto) {
		this.preencherCampoClear(campoComentario, texto);
	}
	
	public void enviar() {
		this.btnEnviar.click();
	}
	
	public void autenticarFormulario(String texto) {
		this.aguardarElementoVisivel(verificarResultado);
		this.esperaForcada();
		Assert.assertEquals(texto, verificarResultado.getText());
	}
	
	
	
	
	@Test
	public void verificaResultado() {
		acessa.AcessarURL();
		selecionarTipoFormulario();
		selecionarFormulario();
		campoNome("teste");
		campoComentario("teste");
		enviar();
		autenticarFormulario("Form submited Successfully!");
	}
	
	
	

}
