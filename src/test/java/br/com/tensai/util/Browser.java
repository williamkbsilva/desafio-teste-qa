package br.com.tensai.util;

public class Browser {

	public static final String FIREFOX = "firefox";
	public static final String CHROME = "chrome";
	public static final String IE = "ie";
	public static final String PHANTOMJS = "phantomjs";
	public static final String REMOTEFIREFOX = "fremote";
	public static final String REMOTECHROME = "cremote";
}
