package br.com.tensai.core;

import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public abstract class CoreBaseTest {

	public WebDriver driver;
	protected ExtentReports extent;
	protected ExtentTest logger;

	protected CoreBaseTest() {
		this.driver = CoreDriver.getDriver();
	}

	/*
	 * metodo executado antes de cada caso de teste
	 */
//	@AfterTest
//	public void after() {
//		if (driver != null) {
//			driver.close();
//			driver.quit();
//		}
//		driver = null;
//	}
//	

}
