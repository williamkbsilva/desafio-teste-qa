package Extents;

import java.io.File;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentsReportsClass {

	public WebDriver driver;

	protected ExtentReports extent;
	protected ExtentTest logger;

	@BeforeTest
	public void startTest() {
		extent = new ExtentReports(System.getProperty("user.dir") + "/test-output/STMExtentReport.html", true);
		extent.addSystemInfo("Host name", "Testing software").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Rajkumar SM");
		extent.loadConfig(new File(System.getProperty("user.dir") + "\\extent-config.xml"));
	}

//	@Test
	public void passTest() {
		logger = extent.startTest("teste passou", "testador: william kevin");
		Assert.assertTrue(true);
		//logger.log(LogStatus.PASS, "teste passou");
	}

	//@Test
	public void failTest() {
		logger = extent.startTest("teste ", "testador: william kevin");
		//Assert.assertTrue(false);
		//logger.log(LogStatus.FAIL, "teste");
	}

	// @Test
	public void skipTest() {
		logger = extent.startTest("skipTest");
		throw new SkipException("Skipping - This is not ready for testing ");

	}

	@AfterMethod
	public void getResult(ITestResult result) {

		if (result.getStatus() == ITestResult.FAILURE) {
			this.failTest();
			logger.log(LogStatus.FAIL, "O Teste " + result.getName() + " falhou ");
			logger.log(LogStatus.FAIL, "motivo do erro: " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			this.passTest();
			logger.log(LogStatus.PASS, "O teste " + result.getName() + " passou");
		}
		
		extent.flush();
		

	}

	// @AfterTest
	public void endReport() {
		
		extent.endTest(logger);
	}

}
